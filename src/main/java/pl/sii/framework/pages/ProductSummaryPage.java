package pl.sii.framework.pages;

import lombok.extern.slf4j.Slf4j;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import pl.sii.framework.base.component.Page;

/**
 * Created by gooornik on 13.09.2019.
 */
public class ProductSummaryPage extends Page {

    @FindBy(id = "myModalLabel")
    WebElement labelInfoInPopup;

    public ProductSummaryPage(WebDriver driver) {
        super(driver);
    }

    public boolean isCorrectPopupDisplayed() {
        log.info("Check if - product added to cart popup - is displayed");
        webDriverWait.until(webDriver -> labelInfoInPopup.isDisplayed());
        return(labelInfoInPopup.getText().contains("successfully added"));
    }
}
