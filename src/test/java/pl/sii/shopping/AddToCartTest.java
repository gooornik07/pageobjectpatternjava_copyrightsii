package pl.sii.shopping;

import io.qameta.allure.Feature;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.parallel.Execution;
import org.junit.jupiter.api.parallel.ExecutionMode;
import pl.sii.base.BaseTest;

import static org.assertj.core.api.Java6Assertions.assertThat;

/**
 * Created by gooornik on 13.09.2019.
 */

@Execution(ExecutionMode.SAME_THREAD)
public class AddToCartTest extends BaseTest {

    @Test
    @Feature("Add product to cart with selected quantity two")
    public void popupShouldBeCorrectWhenAddingProductWithQuantityTwo(){
        assertThat(
                application.open()
                        .searchForProduct("Hummingbird")
                        .selectItemByName("Hummingbird Printed Sweater")
                        .selectQuantity("2")
                        .clickAddToCartButton()
                        .isCorrectPopupDisplayed())
                .isTrue()
                .withFailMessage("Popup is not displayed with correct message");
    }

}
