/*
 * Copyright (c) 2019.  Sii Poland
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *  http://www.apache.org/licenses/LICENSE-2.0
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package pl.sii.framework.pages;

import lombok.extern.slf4j.Slf4j;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import pl.sii.framework.base.component.Page;

import java.util.List;

@Slf4j
public class MainPage extends Page {

    @FindBy(css = "#_desktop_user_info > div > a")
    WebElement signInLink;

    @FindBy(xpath = "//input[@name='s']")
    WebElement searchBox;

    @FindBy(xpath = "//div[@class='product-description']/h2/a")
    List<WebElement> listedItems;

    @FindBy(xpath = "//input[@id='quantity_wanted']")
    WebElement quantityField;

    @FindBy(xpath = "//div[@class='add']/button[@class='btn btn-primary add-to-cart']")
    WebElement addToCartButton;

    @FindBy(xpath = "//div[@class='user-info']/a[@class='logout hidden-sm-down']")
    WebElement signOutButton;

    public MainPage(WebDriver driver) {
        super(driver);
    }

    public SignInPage signIn() {
        log.info("Go to 'Sign in' page");
        signInLink.click();
        return pageFactory.create(SignInPage.class);
    }

    public MainPage searchForProduct(String search){
        log.info("Search for product using textbox");
        searchBox.sendKeys(search);
        searchBox.sendKeys(Keys.ENTER);
        return this;
    }

    public MainPage selectItemByName(String itemName) {
        log.info("Select one item from listed products using itemName");
        for (WebElement item : listedItems) {
            if (item.getText().contains(itemName)) {
                item.click();
                break;
            }
        }
        return this;
    }

    public MainPage selectQuantity(String value){
        log.info("Select quantity for displayed product");
        webDriverWait.until(webDriver -> quantityField.isDisplayed());
        quantityField.clear();
        quantityField.sendKeys(value);
        return this;
    }

    public ProductSummaryPage clickAddToCartButton(){
        log.info("Click ADD TO CART button next to quantity dropdown");
        addToCartButton.click();
        return pageFactory.create(ProductSummaryPage.class);
    }

    public MainPage clickSignOut(){
        signOutButton.click();
        return this;
    }

}